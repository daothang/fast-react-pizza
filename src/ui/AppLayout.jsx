import React from "react";
import Header from "./Header.jsx";
import Cart from "../featueres/cart/Cart.jsx";
import { Footer } from "./Footer.jsx";
import { Outlet, useNavigate, useNavigation } from "react-router-dom";
import Loader from "./Loader.jsx";
import SearchOrder from "../featueres/order/SearchOrder.jsx";

function AppLayout() {
  const navigation = useNavigation();
  const isLoading = navigation.state === "loading";
  return (
    <div className="grid h-screen grid-rows-[auto_1fr_auto]">
      {isLoading && <Loader />}
      <Header />
      <div className="overflow-auto">
        <main className="mx-auto max-w-3xl">
          <Outlet />
        </main>
      </div>
      <Footer />
    </div>
  );
}

export default AppLayout;
