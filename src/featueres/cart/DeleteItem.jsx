import React from "react";
import Button from "../../ui/Button.jsx";
import { useDispatch } from "react-redux";
import { deleteItem } from "./cartSlice.js";

const DeleteItem = ({ pizzaId }) => {
  const dispatch = useDispatch();
  function handleDeleteItem() {
    if (!pizzaId) return;
    dispatch(deleteItem(pizzaId));
  }
  return (
    <Button onClick={handleDeleteItem} type="small">
      Delete
    </Button>
  );
};

export default DeleteItem;
