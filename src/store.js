import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./featueres/user/userSlice.js";
import cartReducer from "./featueres/cart/cartSlice.js";

const store = configureStore({
  reducer: {
    user: userReducer,
    cart: cartReducer,
  },
});

export default store;
